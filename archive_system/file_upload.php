<?php

function render_file_upload() {
        echo 'Upload win32: <input type="file" name="win32" id="win32" />';
		echo 'Upload win64: <input type="file" name="win64" id="win64" />';
}

//this function loops through the file uploads
function update_upload(&$url_ending) {
	$download_items = array('win32', 'win64');
	foreach ($download_items as $download_item) {
		update_upload_single($url_ending, $download_item);
	}
}

//this function processes a single file upload
function update_upload_single(&$url_ending, $download_item) {
	$download_item = $_FILES[$download_item];
    // Make sure our flag is in there, otherwise it's an autosave and we should bail.
    // HANDLE THE FILE UPLOAD
    // If the upload field has a file in it
    if(isset($download_item) && ($download_item['size'] > 0)) {
        // Get the type of the uploaded file. This is returned as "type/extension"
        $arr_file_type = wp_check_filetype(basename($download_item['name']));
        $uploaded_file_type = $arr_file_type['type'];

        // Set an array containing a list of acceptable formats
        $allowed_file_types = array('');

        // If the uploaded file is the right format
        if(in_array($uploaded_file_type, $allowed_file_types)) {
            // Options array for the wp_handle_upload function. 'test_upload' => false
            $upload_overrides = array( 'test_form' => false ); 

            // Handle the upload using WP's wp_handle_upload function. Takes the posted file and an options array
            $uploaded_file = wp_handle_upload($download_item, $upload_overrides);
			echo(var_dump($uploaded_file));
			
			switch ($download_item) {
			case 'win32':
				$new_name_ending = 'PowerMode (Version ' . $_REQUEST['version'] . ') 32-Bit Installer.exe';
			break;
			case 'win64':
				$new_name_ending = 'PowerMode (Version ' . $_REQUEST['version'] . ') 64-Bit Installer.exe';
			break;
			}
            $new_name = dirname($uploaded_file['file']) . '/' . $new_name_ending;

            rename($uploaded_file['file'], $new_name);

            //change attributes for uploaded_file variable to correspond with new file name

            $file_name_and_location = $uploaded_file['file'];

            // If the wp_handle_upload call returned a local path for the image
            if(isset($uploaded_file['file'])) {
				//echo("There is a file!");

                // The wp_insert_attachment function needs the literal system path, which was passed back from wp_handle_upload
                $file_name_and_location = $uploaded_file['file'];

                $url_ending = date('Y') . '/' . date('m') . '/' . $new_name_ending;

                // Generate a title for the image that'll be used in the media library
                //$file_title_for_media_library = 'your title here';

                // Set up options array to add this file as an attachment
                /*$attachment = array(
                    'post_mime_type' => $uploaded_file_type,
                    'post_title' => 'Uploaded image ' . addslashes($file_title_for_media_library),
                    'post_content' => '',
                    'post_status' => 'inherit'
                );

                // Run the wp_insert_attachment function. This adds the file to the media library and generates the thumbnails. If you wanted to attch this image to a post, you could pass the post id as a third param and it'd magically happen.
                $attach_id = wp_insert_attachment( $attachment, $file_name_and_location );
                require_once(ABSPATH . "wp-admin" . '/includes/image.php');
                $attach_data = wp_generate_attachment_metadata( $attach_id, $file_name_and_location );
                wp_update_attachment_metadata($attach_id,  $attach_data);

                // Before we update the post meta, trash any previously uploaded image for this post.
                // You might not want this behavior, depending on how you're using the uploaded images.
                $existing_uploaded_image = (int) get_post_meta($post_id,'_xxxx_attached_image', true);
                if(is_numeric($existing_uploaded_image)) {
                    wp_delete_attachment($existing_uploaded_image);
                }

                // Now, update the post meta to associate the new image with the post
                update_post_meta($post_id,'_xxxx_attached_image',$attach_id);

                // Set the feedback flag to false, since the upload was successful
                $upload_feedback = false;*/


            } else { // wp_handle_upload returned some kind of error. the return does contain error details, so you can use it here if you want.

                $upload_feedback = 'There was a problem with your upload.';
                update_post_meta($post_id,'_xxxx_attached_image',$attach_id);

            }

        } else { // wrong file type

            //$upload_feedback = 'Please upload only image files (jpg, gif or png).';
            //update_post_meta($post_id,'_xxxx_attached_image',$attach_id);

        }

    } else { // No file was passed

        $upload_feedback = false;

    }

    // Update the post meta with any feedback
    //update_post_meta($post_id,'_xxxx_attached_image_upload_feedback',$upload_feedback);

}
