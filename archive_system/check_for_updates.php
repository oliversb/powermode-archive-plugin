<?php

function check_for_updates_shortcode() {
	//get latest version
	global $wpdb;
    
    $table_name = $wpdb->prefix . 'powermode_archived_versions';
    $latest = $wpdb->get_results("SELECT * FROM $table_name ORDER BY version DESC LIMIT 1")[0];
	
	//get version and OS
	$app_version = $_GET['version'];
	$os = $_GET['os'];
	
	if ($latest->version == $app_version) {
		$message = "PowerMode is up to date on your system.";
	} else {
		if ($os == "win32") {
			$link = archive_get_latest_version_win32_shortcode();
		} elseif ($os == "win64") {
			$link = archive_get_latest_version_win64_shortcode();
		}
		$message = "Newer version detected. Upgrade to " . $latest->version . " by <a href='$link'>clicking here</a>.";
	}
	
    return $message . "<br><i>Please note that with further growth for this app, we will be able to implement an improved update system. We hope to make this a better experience soon!</i>";
}

add_shortcode_auto("check_for_updates");