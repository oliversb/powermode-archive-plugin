<?php

function archive_get_version($table_row) {
    return $table_row['version'];
}

function archive_get_dev_stage($table_row) {
    return $table_row['dev_stage'];
}

function archive_get_release_notes_url($table_row) {
    return get_bloginfo('wpurl') . '/powermode-release-notes-version-' . str_replace('.', '-', $table_row['version']);
}

function archive_get_download_win32($table_row) {
    return get_site_url() . '/wp-content/uploads/' . $table_row['download_win32'];
}

function archive_get_download_win64($table_row) {
    return get_site_url() . '/wp-content/uploads/' . $table_row['download_win64'];
}


function archive_get_latest_version_win32_shortcode() {
    global $wpdb;
    
    $table_name = $wpdb->prefix . 'powermode_archived_versions';
    //get latest release and return download link
    return get_site_url() . '/wp-content/uploads/' . $wpdb->get_results("SELECT download_win32 FROM $table_name ORDER BY version DESC LIMIT 1")[0]->download_win32;
}

add_shortcode_auto('archive_get_latest_version_win32');

function archive_get_latest_version_win64_shortcode() {
    global $wpdb;
    
    $table_name = $wpdb->prefix . 'powermode_archived_versions';
    //get latest release and return download link
    return get_site_url() . '/wp-content/uploads/' . $wpdb->get_results("SELECT download_win64 FROM $table_name ORDER BY version DESC LIMIT 1")[0]->download_win64;
}

add_shortcode_auto('archive_get_latest_version_win64');
