<?php
/** 
  * AquaticPrime PHP Config
  * Configuration for web server license generation
  * @author Lucas Newman, Aquatic
  * @copyright Copyright &copy; 2005 Lucas Newman
  * @license http://www.opensource.org/licenses/bsd-license.php BSD License
  */

// ----CONFIG----

// When pasting keys here, don't include the leading "0x" that AquaticPrime Developer adds.
$key = "0xE9492773606B2CA94C4684134F9CBE80153431D0569B9F52B0DCD86F12FDAC3BFA79D8049F1D200E0AB2AB9E4E8E9CD74E4A495F37AA04D76DEE815E04374B8811904388B2AEA35BF1F764E0193DFDD6EC9C1A48700493AB34415A9B7A1CC7C155815F2B7F3512B14D1AF7907AD50C4787D97B710CD1462E312DF901C133B60F";
$privateKey = "0x9B861A4CEAF21DC632D9AD62351329AAB8CD768AE467BF8C75E8904A0CA91D7D51A6900314BE155EB1CC726989B4688F8986DB94CFC6ADE4F3F4563EAD7A32597035D6C66104789DD4DA497A0BAC192ECD42DAC5B250ADD0EAEDE7DBE347684AAC0B1BCF80C7825D9327224840BAF1B79BD7BF90BCD813A1883A601B91C35DCB";

$domain = "powermodegames.com";
$product = "PowerMode";
$download = "http://$domain/latest-stable-version";

// These fields below should be customized for your application.  You can use ##NAME## in place of the customer's name and ##EMAIL## in place of his/her email
$from = "license@powermodegames.com";
$subject = "$product License For ##NAME##";
$message =
"Hello ##NAME##!<br><br>

To get started with $product, follow these steps:<br>

<ul>
<li>Move this email to the inbox (if it is in the junk mailbox)</li>
<li><a href='http://powermodegames.com/terms-and-conditions'><b>Read the Terms & Conditions by clicking here!</b></a></li>
<li>Download and install PowerMode from this link (if you haven't downloaded the software already): <a href='$download'>Latest Version Of PowerMode.</a></li>
<li>Open the email attatchment provided (make sure it opens with PowerMode).</li>
<li>Install Wamp (this application is used to emulate a web server on your computer and test how games will function on the web). It is really easy to operate with PowerMode! Download link: <a href='http://www.wampserver.com/en/#download-wrapper'>Wamp Download Page.</a></li>
<li>Click 'New Project'</li>
<li>Type a project name</li>
<li>Click 'OK'</li>
<li>From the default tab 'App Preferences', configure PowerMode.</li>
<li>You should now be good to go! PowerMode will remember your configuration for next time. Please do not forget to use <a href='http://$domain/community'>the forums for help.</a> We also host <a href='http://docs.$domain'>a documentation section of the website.</a>
<li>Restart PowerMode for configuration to take affect</li>
<li>Please archive this email for reference!</li>
</ul>

<p>For reference, an exact copy of this email will be sent to the owner of PowerMode. If you have any problems setting up PowerMode, view our <a href='http://$domain/troubleshoot'>troubleshooting guide</a> or email support@powermodegames.com through the same email your license is addressed to.</p>

<br>Thanks,<br>
PowerMode";

// It's a good idea to BCC your own email here so you can have an order history
$bcc = "olisbrown0@gmail.com";

// This is the name of the license file that will be attached to the email
$licenseName = "##NAME##.powermode_license";

// ---KAGI ONLY CONFIG----

$kagiPassword = "534rg9uhn54rg0hg0jHGH3f";


// ---PAYPAL ONLY CONFIG----

// Your PDT authorization token
$auth_token = "AUTH TOKEN HERE";
// Put in a URL here to redirect back to after the transaction
$redirect_url = "http://$domain/thanks.html";
$error_url = "http://$domain/error.html";


// ---ESELLERATE ONLY CONFIG----
// Secret text set up in your eSellerate publisher account
$order_notice_secret = "my secret esellerate string";
// List of eSellerate SKUs that should be processed by AquaticPrime.  Included because things like
// eCDs will come through as a separate SKU, but you probably don't want to run the order through
// AquaticPrime.  Anything not in this list will be ignored.
$aquaticPrimeSKUs = array(
		"SKU1234567890"	
		);
		
		
// ---MYSQL CONFIG----

// Database of registrations
$db_host        = "localhost";
$db_user        = "funkpum";
$db_password    = "56A0qD3nDMWfS8e31I";
$db_name        = "funkpum_wrdp2";

?>
