<?php
/** 
  * AquaticPrime Kagi payment tester
  * Sends test transactions to AquaticPrime Kagi
  * @author Lucas Newman, Aquatic
  * @copyright Copyright &copy; 2005 Lucas Newman
  * @license http://www.opensource.org/licenses/bsd-license.php BSD License
  */

// AquaticPrime Kagi Remote Post System Tester
function license_system_index_shortcode() {
	?><form action="http://powermodegames.com/perform-license" method="post">
			<p>
				Password: <input type="text" name="ACG:Password" /><br />
				Purchaser Name: <input type="text" name="ACG:PurchaserName" /><br />
				Purchaser Email: <input type="text" name="ACG:PurchaserEmail" /><br />
				Product: <select name="ACG:Product">
				<option value="PowerMode (Free License)">PowerMode (Free License)</option>
				<option value="PowerMode (Paid License)">PowerMode (Paid License)</option>
				<option value="PowerMode (Paid License) *PART OF FREE OFFER*">PowerMode (Paid License) *PART OF FREE OFFER*</option>
				</select><br />
				Date: <input type="text" name="ACG:DateProcessed" /><br />
				
				<input type="hidden" name="ACG:TransactionID" value="<? echo rand(); ?>" />
				<input type="hidden" name="ACG:Request" value="Generate" />
				<input type="hidden" name="ACG:InputVersion" value="0201" />
				<input type="hidden" name="ACG:Flags" value="test=1" />
				<input type="submit" />
			</p>
		</form>
<?php
}

add_shortcode_auto("license_system_index");